package com.sk.nbaapp.persistence;

import com.sk.nbaapp.model.Player;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PlayerRepository extends JpaRepository<Player, Long> {

}
