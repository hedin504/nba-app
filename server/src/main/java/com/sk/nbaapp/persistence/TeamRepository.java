package com.sk.nbaapp.persistence;

import com.sk.nbaapp.model.Team;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TeamRepository extends JpaRepository<Team, String> {

}
