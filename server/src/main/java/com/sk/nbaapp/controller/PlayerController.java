package com.sk.nbaapp.controller;

import com.sk.nbaapp.constraint.group.OnCreate;
import com.sk.nbaapp.constraint.group.OnUpdate;
import com.sk.nbaapp.exception.NotFoundException;
import com.sk.nbaapp.model.Player;
import com.sk.nbaapp.persistence.PlayerRepository;
import java.util.List;
import javax.validation.groups.Default;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
@RequestMapping(value = "/players",
    consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
    produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
@RequiredArgsConstructor
@Transactional
public class PlayerController {

    private final PlayerRepository playerRepository;

    @GetMapping
    @Transactional(readOnly = true)
    public List<Player> findAll() {
        return playerRepository.findAll();
    }

    @PostMapping
    public Player create(
        @Validated({OnCreate.class, Default.class}) @RequestBody Player player
    ) {
        return playerRepository.save(player);
    }

    @PutMapping("/{id}")
    public Player update(
        @Validated({OnUpdate.class, Default.class}) @RequestBody Player player,
        @PathVariable("id") Long id
    ) {
        return playerRepository.findById(id)
            .map(p -> {
                BeanUtils.copyProperties(player, p, "id");
                return p;
            })
            .orElseThrow(() -> NotFoundException.formatted("player with id: %d not found", id));
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") Long id) {
        playerRepository.deleteById(id);
    }
}
