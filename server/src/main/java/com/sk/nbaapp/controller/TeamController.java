package com.sk.nbaapp.controller;

import com.sk.nbaapp.exception.NotFoundException;
import com.sk.nbaapp.model.Team;
import com.sk.nbaapp.persistence.TeamRepository;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
@RequestMapping(path = "/teams",
    consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
    produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class TeamController {

    private final TeamRepository teamRepository;

    @GetMapping
    public List<Team> findAll() {
        return teamRepository.findAll();
    }

    @GetMapping("{id}")
    public Team findById(@PathVariable("id") String id) {
        return teamRepository.findById(id)
            .orElseThrow(() -> NotFoundException.formatted("team with id: %s not found", id));
    }
}
