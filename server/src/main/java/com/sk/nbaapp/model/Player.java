package com.sk.nbaapp.model;

import com.sk.nbaapp.constraint.group.OnCreate;
import com.sk.nbaapp.constraint.group.OnUpdate;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import javax.validation.constraints.Pattern;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Range;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)

@Entity
public class Player {

    @Id
    @GeneratedValue
    @NotNull(groups = {OnUpdate.class})
    @Null(groups = {OnCreate.class})
    @EqualsAndHashCode.Include
    private Long id;

    @NotEmpty
    private String fullName;

    @Pattern(regexp = "\\d{0,5}$")
    private String phone;

    @Range(min = 120, max = 300)
    private int height;

    @JoinColumn(name = "team_id", referencedColumnName="team_id")
    @Column(name = "team_id")
    private String team_id;
}
