package com.sk.nbaapp.model;

import java.util.List;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Entity
public class Team {

    @Id
    @EqualsAndHashCode.Include
    private String teamId;
    private String abbreviation;
    private Boolean active;
    private String city;
    private String conference;
    private String division;
    private String firstName;
    private String fullName;
    private String lastName;
    private String siteName;
    private String state;

    @OneToMany(mappedBy = "team_id", fetch = FetchType.EAGER)
    private List<Player> players;
}
