package com.sk.nbaapp.config;

import com.sk.nbaapp.model.Player;
import com.sk.nbaapp.persistence.PlayerRepository;
import com.sk.nbaapp.persistence.TeamRepository;
import com.sk.nbaapp.statsclient.StatsClient;
import java.util.Arrays;
import javax.sql.DataSource;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

@Configuration
public class AppConfig {

    @Bean
    public CommandLineRunner fillAppWithInitData(StatsClient statsClient,
        TeamRepository teamRepository,
        PlayerRepository playerRepository) {
        return args -> {
            teamRepository.saveAll(statsClient.getAllTeams());

            playerRepository.saveAll(Arrays.asList(
                player("Player One", "11111", 200, "atlanta-hawks"),
                player("Player Two", "22222", 200, "atlanta-hawks"),
                player("Player Three", "33333", 190)
            ));
        };
    }

    @Bean
    public DataSource dataSource() {
        EmbeddedDatabaseBuilder builder = new EmbeddedDatabaseBuilder();
        return builder.setType(EmbeddedDatabaseType.H2).build();
    }

    private Player player(String name, String phone, int height, String teamId) {
        return Player.builder()
            .fullName(name)
            .phone(phone)
            .height(height)
            .team_id(teamId)
            .build();
    }

    private Player player(String name, String phone, int height) {
        return player(name, phone, height, null);
    }

}
