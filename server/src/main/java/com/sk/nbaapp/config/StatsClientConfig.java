package com.sk.nbaapp.config;

import com.sk.nbaapp.statsclient.DefaultStatsClient;
import com.sk.nbaapp.statsclient.StatsClient;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class StatsClientConfig {

    @Bean
    public StatsClient xmlStatsClient(RestTemplateBuilder restTemplateBuilder) {
        final RestTemplate restTemplate = restTemplateBuilder
            .rootUri("https://erikberg.com")
            .interceptors((request, body, execution) -> {
                request.getHeaders().add("user-agent", "Mozilla/5.0 (X11; Linux x86_64)");
                return execution.execute(request, body);
            })
            .build();
        return new DefaultStatsClient(restTemplate);
    }
}
