package com.sk.nbaapp.statsclient;

import com.sk.nbaapp.model.Team;
import java.util.List;

public interface StatsClient {

    List<Team> getAllTeams();
}
