package com.sk.nbaapp.statsclient;

import static java.util.Optional.ofNullable;

import com.sk.nbaapp.model.Team;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.web.client.RestOperations;

@RequiredArgsConstructor
public class DefaultStatsClient implements StatsClient {

    private final RestOperations restTemplate;

    @Override
    public List<Team> getAllTeams() {
        String teamsUrl = "/nba/teams.json";

        return ofNullable(restTemplate.getForObject(teamsUrl, Team[].class))
            .map(Arrays::asList)
            .orElse(Collections.emptyList());
    }
}
