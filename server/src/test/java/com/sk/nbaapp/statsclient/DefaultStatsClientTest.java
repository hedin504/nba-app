package com.sk.nbaapp.statsclient;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

import com.sk.nbaapp.config.StatsClientConfig;
import com.sk.nbaapp.model.Team;
import java.util.List;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.client.RestClientTest;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.client.MockRestServiceServer;

@RunWith(SpringRunner.class)
@RestClientTest(StatsClientConfig.class)
public class DefaultStatsClientTest {

    @Autowired
    private DefaultStatsClient client;

    @Autowired
    private MockRestServiceServer server;

    @Test
    public void getAllTeams() {
        //given
        server.expect(requestTo("/nba/teams.json"))
            .andRespond(withSuccess(new ClassPathResource("teams.json"), MediaType.APPLICATION_JSON));
        Team expectedTeam = Team.builder()
            .teamId("atlanta-hawks")
            .abbreviation("ATL")
            .active(true)
            .firstName("Atlanta")
            .lastName("Hawks")
            .conference("East")
            .division("Southeast")
            .siteName("Philips Arena")
            .city("Atlanta")
            .state("Georgia")
            .fullName("Atlanta Hawks")
            .build();

        //when
        final List<Team> allTeams = client.getAllTeams();

        //than
        assertThat(allTeams)
            .isNotNull()
            .hasSize(3)
            .first()
            .isEqualToComparingFieldByField(expectedTeam);

    }
}