import React, {Component} from 'react';
import TextField from '@material-ui/core/TextField/TextField';
import {styles} from "../styles";
import withStyles from '@material-ui/core/styles/withStyles';
import FormControl from '@material-ui/core/FormControl/FormControl';
import InputLabel from '@material-ui/core/InputLabel/InputLabel';
import MenuItem from '@material-ui/core/MenuItem/MenuItem';
import Select from '@material-ui/core/Select/Select';
import {Team} from "../model/Team";
import Button from '@material-ui/core/Button/Button';
import {BackEndClient} from "../utils/BackEndClient";
import {Player} from "../model/Player";
import Paper from '@material-ui/core/Paper/Paper';
import Typography from '@material-ui/core/Typography/Typography';

interface AddPlayerProps {
  teams: Array<Team>;
  classes?: any;
  editablePlayer?: Player;
  fetchPlayers?: Function;
}

interface AddPlayerState {
  player: Player;
  errorMessage?: string;
}

class AddPlayer extends Component<AddPlayerProps, AddPlayerState> {

  state = {
    player: {
      full_name: '',
      phone: '',
      height: 200,
      team_id: undefined
    },
    errorMessage: undefined
  };

  private readonly regexpPhone = new RegExp('^[+0-9]{0,5}$');


  componentDidMount(): void {
    if (this.props.editablePlayer) {
      this.setState({
        player: this.props.editablePlayer
      });
    }
  }

  render(): React.ReactNode {
    const {classes} = this.props;
    const {player} = this.state;
    return (
        <form className={classes.container} noValidate autoComplete="off">
          <TextField
              id="outlined-name"
              label="Full Name"
              className={classes.textField}
              value={player.full_name}
              onChange={this.setFullName}
              margin="normal"
              variant="outlined"
          />
          <TextField
              id="outlined-name"
              label="Phone"
              className={classes.textField}
              value={player.phone}
              onChange={this.setPhone}
              margin="normal"
              variant="outlined"
          />
          <TextField
              id="outlined-name"
              label="Height"
              className={classes.textField}
              value={player.height}
              onChange={this.setHeight}
              margin="normal"
              variant="outlined"
          />
          <FormControl className={classes.formControl}>
            <InputLabel htmlFor="team-id">Team</InputLabel>
            <Select
                value={player.team_id ? player.team_id : 'None'}
                onChange={this.setTeamId}
                inputProps={{
                  name: 'Team',
                  id: 'team-id',
                }}
            >
              <MenuItem value="">
                <em>None</em>
              </MenuItem>
              {this.props.teams.map((team, index) => {
                return (
                    <MenuItem key={index} value={team.team_id}>{team.full_name}</MenuItem>
                );
              })}
            </Select>
          </FormControl>
          <Button variant="contained"
                  className={classes.button}
                  onClick={this.props.editablePlayer ? this.handleUpdatePlayer : this.handleAddPlayer}>
            {this.props.editablePlayer
                ? "SAVE"
                : "ADD"}
          </Button>

          {this.state.errorMessage &&
          <Paper className={classes.root} elevation={1}>
            <Typography component="p">
              {this.state.errorMessage}
            </Typography>
          </Paper>
          }

        </form>

    );
  }

  private readonly handleUpdatePlayer = (event: any) => {
    const {fetchPlayers} = this.props;
    BackEndClient.updatePlayer(this.state.player)
    .then(res => {
          if (res.ok) {
            this.setState({errorMessage: ''})
            if (fetchPlayers) {
              fetchPlayers();
            }
          } else {
            res.json().then(resJson => {
              let msg = '';
              resJson.errors.map((err: any) => {
                msg = `${msg} ${err.field} ${err.default_message}; `;
              });
              this.setState({errorMessage: msg})
            });
          }
        }
    );
  };

  private readonly handleAddPlayer = (event: any) => {
    BackEndClient.addPlayer(this.state.player)
    .then(res => {
      if (res.ok) {
        this.setState({errorMessage: ''});
      } else {
        res.json().then(resJson => {
          let msg = '';
          resJson.errors.map((err: any) => {
            msg = `${msg} ${err.field} ${err.default_message}; `;
          });
          this.setState({errorMessage: msg})
        });
      }
    });
  };

  private readonly setFullName = (event: any) => {
    const fullName = event.target.value;
    this.setState((prevState) => {
      const player = prevState.player;
      player.full_name = fullName;
      return {player}
    });
  };

  private readonly setPhone = (event: any) => {
    const value = event.target.value;
    if (this.regexpPhone.test(value)) {
      this.setState((prevState) => {
        const player = prevState.player;
        player.phone = value;
        return {player}
      });
    }
  };

  private readonly setHeight = (event: any) => {
    const value = event.target.value;

    this.setState((prevState) => {
      const player = prevState.player;
      player.height = value;
      return {player}
    });
  };

  private readonly setTeamId = (event: any) => {
    const team_id = event.target.value !== '' ? event.target.value : null;
    this.setState((prevState) => {
      const player = prevState.player;
      player.team_id = team_id;
      return {player}
    });
  };

}

export default withStyles(styles)(AddPlayer);








