import React, {Component} from 'react';
import {styles} from "../styles";
import withStyles from '@material-ui/core/styles/withStyles';
import {Paper, Table, TableHead, TableRow, TableCell, TableBody, Fab, Icon} from '@material-ui/core';
import {Team} from "../model/Team";
import {Player} from "../model/Player";
import {BackEndClient} from "../utils/BackEndClient";

interface PlayerTableProps {
  players: Array<Player>;
  teams: Array<Team>;
  classes?: any;
  fetchPlayers: Function;
  handleEditClick: Function;
}

class PlayerTable extends Component<PlayerTableProps> {


  render() {
    const {classes, players} = this.props;

    return (
        <div>
          <Paper className={classes.root}>
            <Table className={classes.table}>
              <TableHead>
                <TableRow>
                  <TableCell>Full Name</TableCell>
                  <TableCell>Phone</TableCell>
                  <TableCell>Height, cm</TableCell>
                  <TableCell>Team</TableCell>
                  <TableCell>Actions</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {players.map((player, index) => {
                  return (
                      <TableRow key={index}>
                        <TableCell component="th" scope="row">
                          {player.full_name}
                        </TableCell>
                        <TableCell>{player.phone}</TableCell>
                        <TableCell>{player.height}</TableCell>
                        <TableCell>
                          {player.team_id ? player.team_id : '---'}
                        </TableCell>
                        <TableCell>
                          <Fab color="secondary"
                               aria-label="Edit"
                               className={classes.fab}
                               size={"small"}
                               onClick={this.handleEdit(player)}>
                            <Icon>edit_icon</Icon>
                          </Fab>
                          <Fab aria-label="Delete"
                               className={classes.fab}
                               size={"small"}
                               onClick={this.handleDelete(player.id)}>
                            <Icon>delete_icon</Icon>
                          </Fab>
                        </TableCell>
                      </TableRow>
                  );
                })}
              </TableBody>
            </Table>
          </Paper>
        </div>
    );
  }

  private readonly handleEdit = (player: Player) => {
    return (event: any) => {
      this.props.handleEditClick(player);
    }
  };

  private readonly handleDelete = (playerId?: number) => {
    return (event: any) => {
      if (playerId) {
        BackEndClient.deletePlayer(playerId)
        .then(res => {
          if (res.ok) {
            this.props.fetchPlayers();
          }
        });
      }
    }
  }
}

export default withStyles(styles)(PlayerTable);
