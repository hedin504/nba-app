import React, {Component} from 'react';
import {styles} from "../styles";
import withStyles from '@material-ui/core/styles/withStyles';
import {Paper, Table, TableHead, TableRow, TableCell, TableBody} from '@material-ui/core';
import {Team} from "../model/Team";
import {Player} from "../model/Player";

interface TeamTableProps {
  players: Array<Player>;
  teams: Array<Team>;
  classes?: any
}

class TeamTable extends Component<TeamTableProps> {


  render() {
    const {classes, teams} = this.props;

    return (
        <div>
          <Paper className={classes.root}>
            <Table className={classes.table}>
              <TableHead>
                <TableRow>
                  <TableCell>Full Name</TableCell>
                  <TableCell>State</TableCell>
                  <TableCell>City</TableCell>
                  <TableCell>Active</TableCell>
                  <TableCell>Abbreviation</TableCell>
                  <TableCell>Players Q-ty</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {teams.map((team, index) => {
                  return (
                      <TableRow key={index}>
                        <TableCell component="th" scope="row">
                          {team.full_name}
                        </TableCell>
                        <TableCell align="right">{team.state}</TableCell>
                        <TableCell align="right">{team.city}</TableCell>
                        <TableCell align="right">{team.active ? 'Yes' : 'No'}</TableCell>
                        <TableCell align="right">{team.abbreviation}</TableCell>
                        <TableCell align="right">
                          {team.players.length}
                        </TableCell>
                      </TableRow>
                  );
                })}
              </TableBody>
            </Table>
          </Paper>
        </div>
    );
  }

}

export default withStyles(styles)(TeamTable);
