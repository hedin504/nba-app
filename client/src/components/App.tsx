import React, {Component} from 'react';
import {BackEndClient} from "../utils/BackEndClient";
import {Team} from "../model/Team";
import {Player} from "../model/Player";
import {withStyles} from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import {styles} from "../styles";
import PlayerTable from "./PlayerTable";
import TeamTable from "./TeamTable";
import AddPlayer from "./AddPlayer";

interface AppState {
  teams: Array<Team>;
  players: Array<Player>;
  activeComponent: ActiveComponent;
  editablePlayer?: Player
}

interface AppProps {
  classes: any;
}

enum ActiveComponent {
  PLAYERS,
  TEAMS,
  ADD_PLAYER
}

class App extends Component<AppProps, AppState> {

  state = {
    teams: [],
    players: [],
    activeComponent: ActiveComponent.PLAYERS,
    editablePlayer: undefined
  };

  componentDidMount = async () => {
    const teams = await BackEndClient.getTeams();
    const players = await BackEndClient.getPlayers();
    this.setState({
      teams,
      players
    })
  };

  render() {
    const {classes} = this.props;
    return (
        <div className={classes.root}>
          <AppBar position={'static'}>
            <Toolbar>
              <Typography variant="h6" color="inherit">
                NBA APP
              </Typography>
              <Button variant="contained" className={classes.button}
                      onClick={this.handleTeamsClick}>
                Teams
              </Button>
              <Button variant="contained" className={classes.button}
                      onClick={this.handlePlayersClick}>
                Players
              </Button>
              <Button variant="text" className={classes.button}
                      onClick={(e) => this.setState({activeComponent: ActiveComponent.ADD_PLAYER})}>
                Add player
              </Button>
            </Toolbar>
          </AppBar>

          {this.renderSelectedComponent()}
        </div>
    );
  }

  private readonly handleTeamsClick = async (event: any) => {
    if (this.state.activeComponent !== ActiveComponent.TEAMS) {
      const teams = await BackEndClient.getTeams();
      this.setState({
        activeComponent: ActiveComponent.TEAMS,
        teams
      });
    }
  };

  private readonly handlePlayersClick = async (event: any) => {
    if (this.state.activeComponent !== ActiveComponent.PLAYERS) {
      this.fetchPlayers();
    }
  };

  private readonly fetchPlayers = async () => {
    const players = await BackEndClient.getPlayers();
    this.setState({
      activeComponent: ActiveComponent.PLAYERS,
      players
    });
  };

  private readonly handleEditClick = (player: Player) => {
    this.setState({
      activeComponent: ActiveComponent.ADD_PLAYER,
      editablePlayer: player
    });
  };

  private readonly renderSelectedComponent = () => {
    const {players, teams, editablePlayer} = this.state;

    switch (this.state.activeComponent) {
      case ActiveComponent.PLAYERS: {
        return this.renderPlayerTable();
      }
      case ActiveComponent.TEAMS: {
        return <TeamTable players={players} teams={teams}/>;
      }
      case ActiveComponent.ADD_PLAYER: {
        return editablePlayer
            ? <AddPlayer teams={this.state.teams} editablePlayer={editablePlayer} fetchPlayers={this.fetchPlayers}/>
            : <AddPlayer teams={this.state.teams}/>;
      }
      default: {
        return this.renderPlayerTable();
      }
    }
  };

  private readonly renderPlayerTable = () => {
    const {players, teams} = this.state;
    return <PlayerTable
        players={players}
        teams={teams}
        fetchPlayers={this.fetchPlayers}
        handleEditClick={this.handleEditClick}
    />;
  }
}

export default withStyles(styles)(App);


