import {Player} from "./Player";

export interface Team {
  team_id: string;
  abbreviation: string;
  active: boolean;
  city: string;
  conference: string;
  division: string;
  first_name: string;
  full_name: string;
  last_name: string;
  site_name: string;
  state: string;
  players: Array<Player>;
}