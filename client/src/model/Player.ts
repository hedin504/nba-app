export interface Player {
  id?: number;
  full_name: string;
  phone: string;
  height: number;
  team_id?: string;
}