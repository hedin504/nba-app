export const styles = (theme: any) => ({
  root: {
    flexGrow: 1,
    width: '100%',
    marginTop: theme.spacing.unit * 3,

  },
  container: {
    display: 'flex',
    "flex-direction": 'column',
    "flex-wrap": 'wrap',
  },
  grow: {
    flexGrow: 2,
  },
  button: {
    margin: theme.spacing.unit,
    maxWidth: 300,
  },
  input: {
    display: 'none',
  },
  table: {
    minWidth: 700,
  },
  formControl: {
    margin: theme.spacing.unit,
    minWidth: 120,
    maxWidth: 300,
  },
  textField: {
    maxWidth: 300,
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
  },
  fab: {
    margin: theme.spacing.unit,
  },
  extendedIcon: {
    marginRight: theme.spacing.unit,
  },
});
