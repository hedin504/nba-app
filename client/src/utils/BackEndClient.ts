import fetch from 'cross-fetch';
import {Player} from "../model/Player";

export class BackEndClient {

  private static readonly DEFAULT_OPTIONS = {
    body: null,
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    url: null
  };

  public static getTeams = () => {
    const serverUrl = BackEndClient.getServerUrl();
    return fetch(`${serverUrl}/teams`,
        {
          ...BackEndClient.DEFAULT_OPTIONS,
          method: 'GET'
        })
    .then(BackEndClient.handleJsonResponse);
  };

  private static getServerUrl = () => {
    const envBeUrl = process.env.REACT_APP_BACK_URL;
    if (envBeUrl === '' || envBeUrl === undefined) {
      return 'http://localhost:8080'
    }
    return envBeUrl;
  };

  public static getPlayers = () => {
    const serverUrl = BackEndClient.getServerUrl();

    return fetch(`${serverUrl}/players`,
        {
          ...BackEndClient.DEFAULT_OPTIONS,
          method: 'GET'
        })
    .then(BackEndClient.handleJsonResponse);
  };

  public static addPlayer = (player: Player) => {
    const serverUrl = BackEndClient.getServerUrl();

    return fetch(`${serverUrl}/players`,
        {
          ...BackEndClient.DEFAULT_OPTIONS,
          method: 'POST',
          body: JSON.stringify(player)
        });
  };

  public static updatePlayer = (player: Player) => {
    const serverUrl = BackEndClient.getServerUrl();

    return fetch(`${serverUrl}/players/${player.id}`,
        {
          ...BackEndClient.DEFAULT_OPTIONS,
          method: 'PUT',
          body: JSON.stringify(player)
        });
  };

  public static deletePlayer = (playerId: number) => {
    const serverUrl = BackEndClient.getServerUrl();

    return fetch(`${serverUrl}/players/${playerId}`,
        {
          ...BackEndClient.DEFAULT_OPTIONS,
          method: 'DELETE'
        });
  };

  private static handleJsonResponse = (response: Response) => {
    if (!response.ok) {
      return response.text().then((responseText: string) => {
        throw new Error(
            `${response.status} ${response.statusText}\n${responseText}`
        );
      });
    }
    return response.json();
  };
}

