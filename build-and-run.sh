#!/bin/bash
set -e

echo "Build maven project..."
cd ./server && mvn clean install -DskipTests=true && cd ..

echo "Start docker-compose..."
docker-compose build
docker-compose up -d